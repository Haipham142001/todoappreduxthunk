import { Route, Routes } from 'react-router-dom';
import Home from './features/Home';
import Login from './features/User/Login';
import Register from './features/User/Register';

import './scss/Notification.scss';

function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<Home />} exact />
        <Route path='/home' element={<Home />} />
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
      </Routes>
    </>
  );
}

export default App;
