import React from 'react';
import Todo from '../Todo';

const ListTodo = (props) => {
    let listtodo = <tr>
        <td colSpan={6}>
            No task
        </td>
    </tr>;
    if (JSON.stringify(props.todos) !== '[]' && Boolean(props.todos) === true) {
        listtodo = props.todos.map((todo, index) => {
            return <Todo data={todo} key={index} stt={index} onUpdate={(e) => onUpdate(e)}/>
        })
    }

    const onUpdate = (data) => {
        props.onUpdate(data);
    }
    return (
        <tbody>
            {listtodo}
        </tbody>
    )
}

export default ListTodo