import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteTodo } from '../../../redux/TodoSlice';

const Todo = (props) => {
  const todo = props.data;
  const dispatch = useDispatch();

  const handleDeleteTodo = async (index, id) => {
    dispatch(deleteTodo({
      id: id,
      index: index
    }))
  }

  const handleUpdateTodo = (todo) => {
    props.onUpdate(todo);
  }

  return (
    <tr>
      <td>{props.stt + 1}</td>
      <td>{todo.name}</td>
      <td>{todo.dateStart}</td>
      <td>{todo.deadline}</td>
      <td>{todo.status === 0
        ? <input type='checkbox' />
        : <input type='checkbox' defaultChecked />}</td>
      <td>
        <button onClick={() => handleUpdateTodo(todo)}>Sửa</button>
        <button onClick={() => handleDeleteTodo(props.stt, todo.id)}>Xóa</button>
        <div id="toast"></div>
      </td>
    </tr>
  )
}

export default Todo