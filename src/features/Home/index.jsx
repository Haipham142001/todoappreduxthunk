import React, { useEffect, useState } from 'react';
import Header from '../components/Header';
import { useSelector, useDispatch } from 'react-redux';
import ListTodo from './ListTodo';
import Notification from '../../services/Notification';
import { addTodo, getTodo, updateTodo } from '../../redux/TodoSlice';

const Home = () => {

    const store = useSelector(state => state.todos);
    const [name, setName] = useState('');
    const [dateStart, setDateStart] = useState('');
    const [deadline, setDeadline] = useState('');
    const [isUpdate, setIsUpdate] = useState(false);
    const [todoUpdate, setTodoUpdate] = useState({});
    const dispatch = useDispatch();

    useEffect(() => {
        if (localStorage.getItem('user')) {
            dispatch(getTodo());
        };
    }, [dispatch])


    const hanldeAddTodo = async () => {
        if (name !== '' && dateStart !== '' && deadline !== '') {
            const data = {
                name: name,
                deadline: deadline,
                dateStart: dateStart,
                status: false
            }
            if (isUpdate === true) {
                data.id = todoUpdate.id;
                dispatch(updateTodo(data));
                setIsUpdate(false);
            } else {
                dispatch(addTodo(data));
            }
            setName('');
            setDateStart('');
            setDeadline('');
        } else {
            if (name === '') {
                Notification('Thông báo', 'Thiếu name', 'error');
            };
            if (deadline === '') {
                Notification('Thông báo', 'Thiếu deadline', 'error');
            }
            if (dateStart === '') {
                Notification('Thông báo', 'Thiếu dateStart', 'error');
            }
        }

    }

    const onHanldeUpdateTodo = (todo) => {
        setName(todo.name);
        setDateStart(todo.dateStart);
        setDeadline(todo.deadline);
        setIsUpdate(true);
        setTodoUpdate(todo);
    }

    return (
        <div>
            <Header />
            <div id="toast"></div>
            <div className='login-form'>
                <h1>{isUpdate === true ? 'Update' : 'Add'}</h1>
                <input type='text' placeholder='name' value={name} onChange={(e) => setName(e.target.value)} />
                <input placeholder='date start' value={dateStart} onChange={(e) => setDateStart(e.target.value)} />
                <input type="text" placeholder='deadline' value={deadline} onChange={(e) => setDeadline(e.target.value)} />
                <br />
                <button onClick={hanldeAddTodo}>
                    {isUpdate === true ? 'Update' : 'Add'}
                </button>
            </div>
            <table border={1}>
                <thead>
                    <tr>
                        <th>
                            STT
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Date start
                        </th>
                        <th>
                            Deadline
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <ListTodo todos={store.todos} onUpdate={(e) => onHanldeUpdateTodo(e)} />
            </table>
        </div>
    )
}

export default Home