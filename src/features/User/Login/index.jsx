import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

import '../../../scss/login.scss';
import fetchApi from '../../../services/api';
import { API } from '../../../constant/GLOBAL';
import Notification from '../../../services/Notification';
import publicIp from 'react-public-ip';
import { useNavigate } from 'react-router-dom';

const Login = () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [verifyCode, setVerifyCode] = useState('');
    const [isVerify, setIsVerify] = useState(false);
    const [email, setEmail] = useState(localStorage.getItem('email') || '');
    const navigate = useNavigate();

    const handleLogin = async () => {
        const useragent = await navigator.userAgent;
        localStorage.setItem('device', useragent);
        (async () => {
            return await publicIp.v4(); // for public ip v4
            // console.log(await publicIp.v6()); // for public ip v6   
        })()
            .then(async res => await localStorage.setItem('ip', res))

        if (username !== '' && password !== '') {
            let result;
            if (isVerify === false) {
                result = await fetchApi(API.login, "POST", {
                    username: username,
                    password: password,
                    device: localStorage.getItem('device'),
                    ip: localStorage.getItem('ip'),
                });
            } else {
                result = await fetchApi(API.login, "POST", {
                    username: username,
                    password: password,
                    device: localStorage.getItem('device'),
                    ip: localStorage.getItem('ip'),
                    verifyCode: verifyCode
                });
            }

            if (result.message) {
                if (result.message.toLowerCase().indexOf('Phát hiện đăng nhập') === -1) {
                    Notification('Thông báo', result.message, 'error');
                    setIsVerify(true);
                    setEmail(result.type);
                    localStorage.setItem('email', result.type);
                } else {
                    Notification('Thông báo', result.message, 'error');
                    return;
                }
            } else {
                localStorage.setItem("authKey", result.token);
                localStorage.setItem("user", JSON.stringify(result.user));
                Notification('Thông báo', 'Đăng nhập thành công', 'success');
                setTimeout(() => {
                    navigate('/home');
                }, 2000);
            }
        } else {
            return Notification('Thông báo', 'Thiếu username hoặc password', 'error');
        }
    }

    return (
        <div className='login'>
            <div id="toast"></div>
            {isVerify === false
                ? <div className='login-form'>
                    <h1>Login</h1>
                    <div className='input-form'>
                        <TextField
                            label="Username"
                            variant="outlined"
                            fullWidth
                            value={username}
                            onChange={(e) => setUsername(e.target.value)} />
                    </div>
                    <div className='input-form'>
                        <TextField
                            label="Password"
                            variant="outlined"
                            type='password'
                            fullWidth
                            value={password}
                            onChange={(e) => setPassword(e.target.value)} />
                    </div>
                    <Button
                        variant="contained"
                        color="success"
                        onClick={handleLogin}
                    >
                        Login
                    </Button>
                </div>
                : <div className='login-form verify'>
                    <h1>Verify</h1>
                    <h2>Hệ thống đã gửi mã xác thực tới</h2>
                    <p>{email || localStorage.getItem('email')}</p>
                    <div className='input-form'>
                        <TextField
                            label="Mã xác thực"
                            variant="outlined"
                            type='text'
                            fullWidth
                            value={verifyCode}
                            onChange={(e) => setVerifyCode(e.target.value)} />
                    </div>
                    <Button
                        variant="contained"
                        color="success"
                        onClick={handleLogin}
                    >
                        Verify
                    </Button>
                </div>}
        </div>
    )
}

export default Login