import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import fetchApi from '../../../services/api';
import { API } from '../../../constant/GLOBAL';
import Notification from '../../../services/Notification';
import { useNavigate } from 'react-router-dom';

const Register = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const navigate = useNavigate();

  if (localStorage.getItem('user')) {
    navigate('/home');
  }

  const handleRegister = async () => {
    if (username !== '' && password !== '' && name !== '') {
      try {
        const result = await fetchApi(API.register, "POST", {
          username: username,
          password: password,
          "name": name,
          "email": email
        });

        if (result.message.toLowerCase().indexOf('thành công') === -1) {
          Notification('Thông báo', result.message, 'error');
        } else {
          Notification('Thông báo', result.message, 'success');
        }
      } catch (e) {
        alert('Lỗi hệ thống');
      }
    }
  }

  return (
    <>
      <div id="toast"></div>
      <div className='login'>
        <div className='login-form'>
          <h1>Register</h1>
          <div className='input-form'>
            <TextField
              label="Username"
              variant="outlined"
              fullWidth
              value={username}
              onChange={(e) => setUsername(e.target.value)} />
          </div>
          <div className='input-form'>
            <TextField
              label="Password"
              variant="outlined"
              type='password'
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)} />
          </div>
          <div className='input-form'>
            <TextField
              label="Name"
              variant="outlined"
              type='text'
              fullWidth
              value={name}
              onChange={(e) => setName(e.target.value)} />
          </div>
          <div className='input-form'>
            <TextField
              label="Email"
              variant="outlined"
              type='text'
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)} />
          </div>
          <Button
            variant="contained"
            color="success"
            onClick={handleRegister}
          >
            Register
          </Button>
        </div>
      </div>
    </>
  )
}

export default Register