import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <ul>
        <li>
          <Link to='/home'>Home</Link>
          {localStorage.getItem('user') ? '' : <Link to='/login'>Login</Link>}
          {localStorage.getItem('user') ? '' : <Link to='/register'>Register</Link>}
        </li>
      </ul>
  )
}

export default Header