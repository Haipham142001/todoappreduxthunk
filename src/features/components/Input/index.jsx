import React from 'react'

const InputForm = (props) => {
    return (
        <input
            type={props.type}
            placeholder={props.placeholder}
            className={props.class}
        />
    )
}

export default InputForm