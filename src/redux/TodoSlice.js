import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import fetchApi from '../services/api';
import { API } from '../constant/GLOBAL';

export const getTodo = createAsyncThunk('todo/getData', async (arg, { rejectWithValue }) => {
    try {
        const value = await fetchApi(API.listTodo, "POST", {});
        return value.todos;
    } catch (error) {
        rejectWithValue(error.response.data);
    }
})

export const addTodo = createAsyncThunk('todo/add', async (arg, { rejectWithValue }) => {
    try {
        const todo = await fetchApi(API.createTodo, "POST", arg);
        return todo.todo
    } catch (error) {
        rejectWithValue(error.response.data);
    }
})

export const updateTodo = createAsyncThunk('todo/update', async (arg, { rejectWithValue }) => {
    try {
        const todo = await fetchApi(API.updateTodo, "POST", arg);
        return todo;
    } catch (error) {
        rejectWithValue(error.response.data);
    }
})

export const deleteTodo = createAsyncThunk('todo/delete', async (arg, { rejectWithValue }) => {
    try {
        await fetchApi(API.deleteTodo, "POST", { id: arg.id });
        return arg.index;
    } catch (error) {
        rejectWithValue(error.response.data);
    }
})

export const TodoSlice = createSlice({
    name: 'todo',
    initialState: {
        todos: []
    },
    reducers: {},
    extraReducers: {
        [getTodo.fulfilled]: (state, action) => {
            state.todos = action.payload;
        },
        [deleteTodo.fulfilled]: (state, action) => {
            state.todos.splice(action.payload, 1);
        },
        [addTodo.fulfilled]: (state, action) => {
            state.todos.push(action.payload);
        },
        [updateTodo.fulfilled]: (state, action) => {
            const index = state.todos.findIndex((todo) => {
                return todo.id === action.payload.todo[0].id;
            })
            state.todos[index] = action.payload.todo[0];
        }
    }
})

// Action creators are generated for each case reducer function
// export const { addTodo, listTodo, deleteTodo, updateTodo } = TodoSlice.actions

export default TodoSlice.reducer